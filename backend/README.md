<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## How To Running

- Before running laravel, be aware to installation dependency using composer with typing command.  
`$ composer install`
- After installing dependencies run the following commands:  
`$ cp .env.example .env` -> create file .env copying from .env.example  
`$ php artisan key:generate` -> generate key  
`$ php artisan migrate` -> migration schema to database  
`$ php artisan storage:link` -> to create storage link public  
`$ php artisan jwt:secret` -> to generate jwt secret  
`$ php artisan optimize:clear` -> to optimize laravel  
- Then you can start your server using this command:  
`$ php -S localhost:8000` or `$ php artisan serve`
- And open a browser then visit `http://localhost:8000`

## Deploying in shared hosting

- create file *.htaccess* and adding this command:

    ```cmd
    <IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteRule ^(.*)$ public/$1 [L]
    </IfModule>

    #Disable index view
    options -Indexes

    #hide a Specifuc File

    <Files .env>
    order allow,deny
    Deny from all
    </Files>
    ```

## Entity Relationship Diagram
[Location Folder File](../backend/docs/ERD_career_kyoob.drawio), Open the file using website https://www.draw.io/