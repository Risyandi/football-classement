<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FootballScore extends Model
{
    use HasFactory;

    /**
     * excluded field to mass assignment
     */
    protected $guarded = [];

    /**
     * relations with type goods
     */
    public function clubHome()
    {
        return $this->belongsTo(FootballClub::class, 'club_home_id');
    }

    public function clubAway()
    {
        return $this->belongsTo(FootballClub::class, 'club_away_id');
    }
}
