<?php

namespace App\Http\Controllers;

use App\Models\FootballScore;
use Illuminate\Http\Request;

class FootballScoreController extends Controller
{
    /**
     * get all the data with pagination 10 data to show
     */
    public function index()
    {
        try {
            $footballScore = FootballScore::with('clubHome', 'clubAway')->paginate(10);
            return response()->json($footballScore, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list football score', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * get the data by id
     */
    public function show($id)
    {
        try {
            $footballScore = FootballScore::findOrFail($id);
            return response()->json($footballScore, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list by id football score', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data to database
     */
    public function store(Request $request)
    {
        try {
            $footballScore = FootballScore::create($request->all());
            return response()->json($footballScore, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created football score', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data massive to database 
     */
    public function storeMultiple(Request $request)
    {
        $postsData = $request->all();
        try {
            foreach ($postsData['footballScore'] as $postData) {
                FootballScore::create($postData);
            }
            return response()->json(['message' => 'created successfully'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created mass football score', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * updated the data by id
     */
    public function update(Request $request, $id)
    {
        try {
            $footballScore = FootballScore::findOrFail($id);
            $footballScore->update($request->all());
            return response()->json($footballScore);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error updated football score', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * deleted the data by id
     */
    public function destroy($id)
    {
        try {
            $footballScore = FootballScore::findOrFail($id);
            $footballScore->delete();
            return response()->json(['message' => 'deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error deleted football score', 'error' => $e->getMessage()], 500);
        }
    }
}
