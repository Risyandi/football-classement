<?php

namespace App\Http\Controllers;

use App\Models\FootballClub;
use Illuminate\Http\Request;

class FootballClubController extends Controller
{
    /**
     * get all the data with pagination 10 data to show
     */
    public function index()
    {
        try {
            $footballClub = FootballClub::paginate(10);
            return response()->json($footballClub, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list football club', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * get the data by id
     */
    public function show($id)
    {
        try {
            $footballClub = FootballClub::findOrFail($id);
            return response()->json($footballClub, 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error get list by id football club', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data to database
     */
    public function store(Request $request)
    {
        try {
            $footballClub = FootballClub::create($request->all());
            return response()->json($footballClub, 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created football club', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * put the data massive to database 
     */
    public function storeMultiple(Request $request)
    {
        $postsData = $request->all();
        try {
            foreach ($postsData['footballClub'] as $postData) {
                FootballClub::create($postData);
            }
            return response()->json(['message' => 'created successfully'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error created mass football club', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * updated the data by id
     */
    public function update(Request $request, $id)
    {
        try {
            $footballClub = FootballClub::findOrFail($id);
            $footballClub->update($request->all());
            return response()->json($footballClub);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error updated football club', 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * deleted the data by id
     */
    public function destroy($id)
    {
        try {
            $footballClub = FootballClub::findOrFail($id);
            $footballClub->delete();
            return response()->json(['message' => 'deleted successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => 'error deleted football club', 'error' => $e->getMessage()], 500);
        }
    }

}
