<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FootballClubController;
use App\Http\Controllers\FootballScoreController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * endpoint group api club football
 */
Route::prefix('club')->group(function () {
    Route::get('/', [FootballClubController::class, 'index']);
    Route::get('/{id}', [FootballClubController::class, 'show']);
    Route::post('/', [FootballClubController::class, 'store']);
    Route::post('/mass', [FootballClubController::class, 'storeMultiple']);
    Route::put('/{id}', [FootballClubController::class, 'update']);
    Route::delete('/{id}', [FootballClubController::class, 'destroy']);
});

/**
 * endpoint group api score football
 */
Route::prefix('score')->group(function () {
    Route::get('/', [FootballScoreController::class, 'index']);
    Route::get('/{id}', [FootballScoreController::class, 'show']);
    Route::post('/', [FootballScoreController::class, 'store']);
    Route::post('/mass', [FootballScoreController::class, 'storeMultiple']);
    Route::put('/{id}', [FootballScoreController::class, 'update']);
    Route::delete('/{id}', [FootballScoreController::class, 'destroy']);
});
