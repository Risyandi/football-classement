<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFootballScoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('football_scores', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('club_home_id')->nullable();
            $table->bigInteger('club_away_id')->nullable();
            $table->string('club_home_score');
            $table->string('club_away_score');
            $table->string('match');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('football_scores');
    }
}
