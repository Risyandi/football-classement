# football-classement

The repository for the web application of Football Classement. Created for one of requirement to applying jobs in Aptavis.

## Installation intructions

- Open the terminal  and navigate to your desired directory.

## Frontend

The  frontend of the application is built using React, Tailwind. (On Progress)

## Backend

The  backend of the application is built using PHP, Laravel and database using MySQL.
